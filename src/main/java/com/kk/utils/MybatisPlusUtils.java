package com.kk.utils;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import java.lang.reflect.Field;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author fukun
 */
public class MybatisPlusUtils {

    private static Pattern humpPattern = Pattern.compile("[A-Z]");

    public MybatisPlusUtils() {
    }

    public static <T> void notNullField(T T, QueryWrapper<T> wrapper) {
        Field[] var2 = T.getClass().getDeclaredFields();
        int var3 = var2.length;

        for(int var4 = 0; var4 < var3; ++var4) {
            Field field = var2[var4];
            field.setAccessible(true);

            try {
                if (!"serialVersionUID".equals(field.getName()) && field.get(T) != null) {
                    TableId tableId = (TableId)field.getAnnotation(TableId.class);
                    if (tableId != null) {
                        wrapper.eq(tableId.value(), field.get(T));
                    } else {
                        TableField tableField = (TableField)field.getAnnotation(TableField.class);
                        if (tableField != null) {
                            if (tableField.exist()) {
                                wrapper.eq(tableField.value(), field.get(T));
                            }
                        } else {
                            wrapper.eq(humpToLine(field.getName()), field.get(T));
                        }
                    }
                }
            } catch (IllegalAccessException var8) {
                var8.printStackTrace();
            }
        }

    }

    public static String humpToLine(String str) {
        Matcher matcher = humpPattern.matcher(str);
        StringBuffer sb = new StringBuffer();

        while(matcher.find()) {
            matcher.appendReplacement(sb, "_" + matcher.group(0).toLowerCase());
        }

        matcher.appendTail(sb);
        return sb.toString();
    }
}
